public class Empleado {


    //Atributos
    private String cedula;
    private String nombre;
    private String puesto;

    //Metodos

    public Empleado() {
        this.cedula = "";
        this.nombre = "";
        this.puesto = "";
    }

    public Empleado(String cedula, String nombre, String puesto){
        this.nombre=nombre;
        this.puesto=puesto;
        this.cedula=cedula;
    }

    public String getCedula() {
        return cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public String toString(){
        return "Empleado{" + "nombre=" + nombre +
                ", cedula=" + cedula +
                ", puesto=" + puesto +
                '}';


    }





}
